var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]

var i = 0
function start(times) {
    if(i < books.length) {
        readBooksPromise(times, books[i])
        .then(hasil1 => start(hasil1))
        .catch(error => console.log(error))
    }
    i++
}
start(10000)