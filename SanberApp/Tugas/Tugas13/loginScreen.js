import React from 'react';
import {Alert, StyleSheet, Text, Image, View, TextInput,Button } from 'react-native';
 
export default class LoginPage extends React.Component{
    
    render() {
        const {flexDirection, alignItems, justifyContent} = this.state
        const layoutStyle = {flexDirection, justifyContent, alignItems}

        const primaryAxis = flexDirection === 'row' ? 'Horizontal' : 'Vertical'
        const secondaryAxis = flexDirection === 'row' ? 'Vertical' : 'Horizontal'
        
        return (
            <View style={styles.container}>
                <Toggle
                 label={'Primary Axis (flexDirection'}
                 value={flexDirection}
                 options={['Login Screen', 'Register Screen', 'About Screen']}
                 onChange={(option) => this.setState({flexDirection: option})}
                 />

                <Image style={styles.image} source={require('./images/logoSanber.png')} ></Image>
                <TextInput style={styles.username} placeholder="Username" value={this.state.username} onChangeText={(text) => this.setState({ username:text })}></TextInput>
                <TextInput style={styles.password} placeholder="Password" value={this.state.password} secureTextEntry={true} onChangeText={(text) => this.setState({ password: text })} ></TextInput>
                <View style={styles.loginButton}>
                    <Button title="Login" onPress={this._onPressLogin} color="#841584" ></Button>
                </View>
                <View style={styles.loginButton}>
                    <Button title="Cancel" onPress={this._onPressCancel} color="#FFEB3B" ></Button>
                </View>
            </View>
        );
    }
}
 
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor:'#2196F3'
    },
    image:{
        height:100,
        width:100
    },
    username:{
        backgroundColor:'white',
        borderRadius: 5,
        width:'90%',
        padding: 5,
        marginBottom: 10,
    },
    password: {
        backgroundColor: 'white',
        borderRadius: 5,
        width: '90%',
        padding: 5,
        marginBottom: 10,
    },
    loginButton:{
        width:'90%',
        marginBottom: 10,
    }
});