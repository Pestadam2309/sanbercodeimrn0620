import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

import Component from './Latihan/Component/Component'
import YoutubeUI from './Tugas/Tugas12/App'

export default function App() {
  return (
    <YoutubeUI />
    // <Component />
    // <View style={styles.container}>
    //   <Text>Hello World</Text>
    //   <StatusBar style="auto" />
    // </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
