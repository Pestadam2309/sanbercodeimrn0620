console.log("-----------------Soal 1--------------")
// JS Normal
const golden = function goldenFunction(){
    console.log("this is golden!!")
  }
   
  golden()

//   JS ES6
  let golden2 = () => {
      console.log("this is golden!!")
  }


  console.log("-----------------Soal 2--------------")
  // JS Normal
//   const newFunction = function literal(firstName, lastName){
//     return {
//       firstName: firstName,
//       lastName: lastName,
//       fullName: function(){
//         console.log(firstName + " " + lastName)
//         return 
//       }
//     }
//   }
   
//   //Driver Code 
//   newFunction("William", "Imoh").fullName() 

  //   JS ES6
    const newFunction2 = (firstName2, lastName2) => {
         return {
             firstName2, 
             lastName2,
             fullName2 : function(){
             console.log(firstName2 + " " + lastName2)
         }      
    }
    }
       
newFunction2("William", "Imoh").fullName2()

console.log("-----------------Soal 3--------------")

const newObject = {
    firstName3: "Harry",
    lastName3: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
  }

  const {firstName3, lastName3, destination, occupation} = newObject
  console.log(firstName3, lastName3, destination, occupation)

  console.log("-----------------Soal 4--------------")
    const west = ["Will", "Chris", "Sam", "Holly"]
    const east = ["Gill", "Brian", "Noel", "Maggie"]
    const combined = west.concat(east)
    let combinedArray = [...west, ...east]

console.log(combined)
console.log(combinedArray)

console.log("-----------------Soal 5--------------")

const planet = "earth"
const view = "glass"
var before = 'Lorem ' + view + 'dolor sit amet, ' +  
    'consectetur adipiscing elit,' + planet + 'do eiusmod tempor ' +
    'incididunt ut labore et dolore magna aliqua. Ut enim' +
    ' ad minim veniam'

const before2 = `Lorem  ${view}  dolor sit amet consectetur adipiscing elit ${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`

console.log(before)
console.log(before2)