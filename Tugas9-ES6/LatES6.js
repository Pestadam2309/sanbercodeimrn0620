// Normal JS

// var x = 1;
// if (x === 1) {
//     var x = 2;

//     console.log(x);
// }

// console.log(x);

// // ES6

// let y = 1;
//  if (y === 1) {
//      let y = 2;

//      console.log(y)
//  }

//  console.log(y);
//  const number = 42;
//  number = 100;

// Arrow Function Normal JS

// function f() {

// }

// Destruction normal JS
// let studentName = {
//     firstName: 'Peter',
//     lastName: 'Parker'
// };

// const firstName = studentName.firstName;
// const lastName = studentName.lastName;

// console.log(studentName)

// // Destruction ES6 JS
// let studentName2 = {
//     firstName2 : 'Pesta',
//     lastName2 : 'Dameria'
// }

// const {firstName2, lastName2} = studentName2

// console.log(studentName2)

const fullName = 'Zell Liew'
 
const Zell = {
  fullName: fullName
}
 
// ES6 way
const Zell = {
  fullName
}