// var numbers = [1, 2, 3, 4, 5]
// var strings = ["a", "i", "u", "e", "o"]
// var array2 = [1, "array", "male", 27]

// console.log(numbers.length)
// console.log(numbers[2])
// console.log(strings[2])
// console.log(array2.length)

// var lastIndexArray2 = array2.length - 1

// console.log(numbers[numbers.length - 1])
// console.log(array2[array2.length - 1])
// console.log(strings[strings.length -1])

// 1. Metode Array Push menambahkan element
// var line = ["andi", "ika", "udin", "abduh"]
// line.push ("windi", "zaki", "ziko")
// console.log(line)

// 2. Metode Array Pop kebalikan dari push mengurangi element
// line.pop()
// console.log(line)
// line.pop()
// console.log(line)
// line.pop()
// console.log(line)

// 3. Metode Array Unshift sama dengan Pop menambhkan Element tetapi dari depan
// line.unshift("marini", "sukiyem")
// console.log(line, "==>> setelah emak-emak menyerang")

// 4. Metode Array Shift sama dengan Pop mengurangi element tetapi dari depan
// line.shift()
// console.log(line)

// line.shift()
// console.log(line, "setelah emak-emak diusir")

// 5. Metode Array Sort mengurutkan secara Ascending
// line.sort()
// console.log(line, "setelah di sort Alfabetis")

// Sort Angka
// var nums =[1, 200, 12, 9, 24, 123]
// nums.sort()
// console.log(nums, "Urutan Angka")

// nums.sort(function (a, b) {
//     return a - b
// })
// console.log(nums, "Ururan Angka Ascending")

// nums.sort(function(a, b) {
//     return b - a
// })
// console.log(nums, "Urutan Angka Descanding")

// 6. Metode Array Slice

// var pizza = ["pepperoni", "cheesse", "bacon", "mozarella"]
// console.log(pizza, "Sebelum di Slice")
// var bacon = pizza.slice(2,3)
// console.log(bacon, "setelah di slice")
// var cheesse = pizza.slice(1, 2)
// console.log(cheesse)

// 7. Metode Array Splice digunakan untuk menambahkan element disembarang tempat sesuai dengan apa yang kita mau

// var fruits =["banana", "orange", "grape"]
// console.log(fruits, "Sebelum ditambah")
// fruits.splice (1, 0, "Apple")
// console.log(fruits, "Sudah ditambahkan Aplle di index 1 tanpa hapus index 1 sebelumnya")
// fruits.splice(1,1, "watermelon")
// console.log(fruits, "mengganti apple dengan watermelon")
// fruits.splice(1,1)
// console.log(fruits, "menghapus watermelon")

// 8. Metode Array Split mengubah String menjadi Array

// var stringSlug = "mobile-app-development"
// var arraySlug = stringSlug.split("-")
// console.log(arraySlug)

// 9. Metode Array Join mengubah Array menjadi String
// var newString = arraySlug.join(" ")
// console.log(newString)

//10. Metode ArrayMultiDimensi
// membuat array menggunakan looping

var tampung = []
for (var i=0; i < 10; i++) {
    tampung.push(i+1)
    console.log(tampung, "Pada Iterasi ke =>", i)
}
console.log(tampung)