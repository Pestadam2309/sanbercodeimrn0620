import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import Home from './AplikasiKamus/Screen/kamus';
import Detail from './AplikasiKamus/Screen/detail';
import About from './AplikasiKamus/Screen/about';
import Splash from './AplikasiKamus/Screen/splash';

const AppNavigator = createStackNavigator(
  {
    Home: Home,
    Detail: Detail,
    About: About,
    Splash: Splash,
  },
  {
    initialRouteName: "Splash",
    headerMode: 'none',
    navigationOptions: {
      headerVisible: false,
    }
  }
);

export default createAppContainer(AppNavigator);
