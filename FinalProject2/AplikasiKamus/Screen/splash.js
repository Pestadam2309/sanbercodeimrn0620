import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { StackActions, NavigationActions } from 'react-navigation';

const resetAction = StackActions.reset({
    index: 0,
    actions: [
        NavigationActions.navigate({ routeName: 'Home'})
    ]
});

class Splash extends Component {
    componentDidMount() {
        setTimeout(() => {
            this.props.navigation.dispatch(resetAction);
        }, 4000);
    }

    render() {
        return(
            <View style={{backgroundColor: '#FFFFFF', flex: 1}}>
                <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                <Text style={{fontSize: 28, fontWeight: 'bold'}}>Welcome To</Text>
                    <Text style={{fontSize: 28, fontWeight: 'bold'}}>Aplication Dictionary</Text>
                    <Text style={{fontSize: 18}}>Kamus Bahasa Indonesia - Inggris</Text>
                </View>
                           
                    <Text style={{textAlign: 'center'}}>Pesta Dameria Tobing</Text>
                    <Text style={{textAlign: 'center'}}>Version 1.0|2020</Text>
                
            </View>
        )
    }
}

export default Splash;