import React, { Component } from 'react';
import { View, StatusBar, Text, TouchableOpacity } from 'react-native';
import DrawerLayout from 'react-native-gesture-handler/DrawerLayout';
import Icon  from 'react-native-vector-icons/FontAwesome5'


class About extends Component {
    renderDrawer = () => {
        return(
            <View style={{marginTop: 20}}>
                <TouchableOpacity style={{flexDirection: 'row', alignItems: 'center', margin: 10}}
                 onPress={() => this.drawerMenuNavigation('Home')}>
                     <View style={{width: 50, height: 25, justifyContent: 'center', alignItems: 'center'}}>
                         <Icon name='home' size={25} color="#607d8b" style={{marginRight: 10}}></Icon>
                     </View>                          
                     <Text style={{fontSize: 18, color:"#607d8b" }}>Home</Text>
                 </TouchableOpacity>
 
                 <TouchableOpacity style={{flexDirection: 'row', alignItems: 'center', margin: 10}}
                     onPress={() => this.drawerMenuNavigation('About')}>                    

                     <View style={{width: 50, height: 25, justifyContent: 'center', alignItems: 'center'}}>
                         <Icon name='user' size={25} color="#900" style={{marginRight: 10}}></Icon>
                    </View>
                <Text style={{fontSize: 18, color: "#900" }}>About Me</Text>
                 </TouchableOpacity>
            </View>            
        )
    }

    drawerMenuNavigation = (route) => {
        this.drawer.closeDrawer();
        this.props.navigation.navigate(route);
    }

    render() {
        return(
            <DrawerLayout
                ref={drawer => {
                    this.drawer =drawer;
                }}
                drawerWidth={250}
                drawerPosition={DrawerLayout.positions.Left}
                drawerType='front'
                drawerBackgroundColor="#FFF"
                renderNavigationView={this.renderDrawer}>

            
                <View style={{flex: 1}}>
                    <StatusBar backgroundColor="#e91e63" barStyle="light-content" />                  

                    <View style={{padding: 20, backgroundColor: "#f48fb1", elevation: 1, flexDirection: 'row', alignItems: 'center'}}>
                        <Icon name='bars' size={30} color="#900" style={{marginRight: 10}} onPress={() => this.drawer.openDrawer()}></Icon>
                        <View style={{flex: 1}}>
                            <Text style={{textAlign: 'center', color: "#FFFFFF", fontWeight: 'bold', fontSize: 18}}>About Me</Text>
                        </View>
                    </View>   

                    <View style={{margin: 10}}>
                        <Text style={{fontWeight: 'bold', fontSize: 18, textAlign: 'center' }}>Pesta Dameria Tobing</Text>
                        <Text style={{fontSize: 16, textAlign: 'center' }}>Peserta Bootcamp React Native Developer</Text>
                        <Text style={{marginTop: 10, textAlign: 'justify'}}>    
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.                     
                        </Text>
                    </View> 

                        
                        
                </View>
           
            </DrawerLayout>
        );
    }
}

   

export default About;