import React, { Component } from 'react';
import { View, StatusBar, Text, Clipboard, ToastAndroid, FlatList} from 'react-native';
import Icon  from 'react-native-vector-icons/FontAwesome5'
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Value } from 'react-native-reanimated';

class Detail extends Component {
    constructor(props) {
        super(props);        
    }

    salin = (value) => {
        Clipboard.setString(value)
        // console.warn(value)
        ToastAndroid.show(value, ToastAndroid.SHORT);
    }

    render() {
        const indonesia = this.props.navigation.getParam('indonesia', 'no-indonesia');
        const english = this.props.navigation.getParam('english', 'no-english');
        return(
            <View style={{flex: 1}}>
                <StatusBar backgroundColor="#e91e63" barStyle="light-content" />
                
                <View style={{padding: 20, backgroundColor: "#f48fb1", elevation: 1, flexDirection: 'row', alignItems: 'center'}}>
                    <Icon name='arrow-circle-left' size={30} color="#900" style={{marginRight: 10}} onPress={() => this.props.navigation.pop()}></Icon>
                    <Text style={{textAlign: 'center', color: "#FFFFFF", fontWeight: 'bold', fontSize: 18}}>{indonesia}</Text>
                </View>
                <Text style={{textAlign: 'center', marginTop: 20, fontWeight: 'bold', fontSize: 20}}>{indonesia}</Text>
                <Text style={{textAlign: 'center', marginTop: 5, fontSize: 16}}>{english}</Text>
                <TouchableOpacity onPress={() => this.salin(english)} style={{justifyContent: 'center', alignItems: 'center', padding: 10, backgroundColor: '#b0bec5', margin: 10, borderRadius: 10}}>
                    <Text style={{textAlign: 'center', fontSize: 16, color: 'white'}}>Salin</Text>
                </TouchableOpacity>

            </View>
        );
    }
}

export default Detail;