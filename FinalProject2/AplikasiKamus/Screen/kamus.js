import React, { Component } from 'react';
import { View, StatusBar, Text, TextInput, FlatList} from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import DrawerLayout from 'react-native-gesture-handler/DrawerLayout';
import Icon  from 'react-native-vector-icons/FontAwesome5'

let database = [
{ indonesia: 'Ayam', english: 'chicken' },
{ indonesia: 'Tikus', english: 'Mouse' },
{ indonesia: 'Kelinci', english: 'Rabbit' },
{ indonesia: 'Anjing', english: 'Dog' },
{ indonesia: 'Babi', english: 'Pig' },
{ indonesia: 'Macan', english: 'Tiger' },
{ indonesia: 'Harimau', english: 'Lion' },
{ indonesia: 'Ular', english: 'Snake' },
{ indonesia: 'Nyamuk', english: 'Mosqueto' },
{ indonesia: 'Semut', english: 'Ant' },
]

class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            text: '',
            data: database
        };

   }

   search = () => {
     let data = database;
     data = data.filter(item => item.indonesia.toLocaleLowerCase().includes(this.state.text.toLocaleLowerCase()));

     this.setState({
       data : data
     })
   }

   drawerMenuNavigation = (route) => {
       this.drawer.closeDrawer();
       this.props.navigation.navigate(route);
   }

   renderDrawer = () => {
       return(
           <View style={{marginTop: 20}}>
               <TouchableOpacity style={{flexDirection: 'row', alignItems: 'center', margin: 10}}
                onPress={() => this.drawerMenuNavigation('Home')}>
                   <View style={{width: 50, height: 25, justifyContent: 'center', alignItems: 'center'}}>
                        <Icon name='home' size={25} color="#900" style={{marginRight: 10}}></Icon>
                   </View>      
                    <Text style={{fontSize: 18, color:"#900" }}>Home</Text>
                </TouchableOpacity>

                <TouchableOpacity style={{flexDirection: 'row', alignItems: 'center', margin: 10}}
                    onPress={() => this.drawerMenuNavigation('About')}>
                    <View style={{width: 50, height: 25, justifyContent: 'center', alignItems: 'center'}}>
                        <Icon name='user' size={25} color="#607d8b" style={{marginRight: 10}}></Icon>
                    </View>
               <Text style={{fontSize: 18, color:"#607d8b" }}>About Me</Text>
                </TouchableOpacity>
           </View>            
       )
   }


    render() {
        return(
            <DrawerLayout
                ref={drawer => {
                    this.drawer =drawer;
                }}
                drawerWidth={250}
                drawerPosition={DrawerLayout.positions.Left}
                drawerType='front'
                drawerBackgroundColor="#FFF"
                renderNavigationView={this.renderDrawer}>

            
                <View style={{flex: 1}}>
                    <StatusBar backgroundColor="#e91e63" barStyle="light-content" />                  

                    <View style={{padding: 20, backgroundColor: "#f48fb1", elevation: 1, flexDirection: 'row', alignItems: 'center'}}>
                        <Icon name='bars' size={30} color="#900" style={{marginRight: 10}} onPress={() => this.drawer.openDrawer()}></Icon>
                        <View style={{flex: 1}}>
                            <Text style={{textAlign: 'center', color: "#FFFFFF", fontWeight: 'bold', fontSize: 18}}>KAMUSKU</Text>
                        </View>
                        
                    </View>

                    <TextInput
                    style={{ height: 40, borderColor: 'gray', borderWidth: 1, paddingLeft: 10, margin: 10, marginVertical:20, marginHorizontal: 10, borderRadius: 10 }}
                    onChangeText={text => this.setState({text: text})}
                    value={this.state.text}
                    placeholder="Masukan Kata Kunci"
                    onKeyPress = {() => this.search()}
                    />

                    <FlatList
                    data={this.state.data}
                    renderItem={({item}) => 
                    <TouchableOpacity style={{borderWidth: 1, borderRadius: 5, marginVertical: 5, marginHorizontal: 20, padding: 10}}
                    onPress= {() => this.props.navigation.navigate('Detail', {indonesia: item.indonesia, english: item.english})}
                    >
                        <Text style={{fontSize: 18, fontWeight: 'bold'}}>{item.indonesia} </Text>
                        <Text style={{fontSize: 16, marginTop: 5}}>{item.english}</Text>
                    </TouchableOpacity>
                    }
                    keyExtractor={item => item.indonesia}
                    />
            </View>
            </DrawerLayout>
        );
    }
}

export default Home;