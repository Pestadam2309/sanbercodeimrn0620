var soal1 = "Soal No. 1"
var soal2 = "Soal No. 2"
var soal3 = "Soal No. 3"
var batas = "=============================================== \n"

console.log(soal1)
console.log(batas)

function teriak() {
    var string = "Halo Sanbers...!"
    return string
}
console.log(teriak())
console.log(batas)

console.log(soal2)
console.log(batas)

function perkalian(num1, num2) {
     return num1 * num2
}

var hasilkali = perkalian(12, 4)
console.log(hasilkali)

console.log(batas)

console.log(soal3)
console.log(batas)

function introduce(name, age, address, hobby) {
    var perkenalan = "Nama saya " + name + ", umur saya " + age + " tahun, alamat saya di " + address + ", dan saya punya hobby yaitu " + hobby
    return perkenalan
}

var perkenalkanDiri = introduce ("Agus", "30", "Jln. Malioboro, Yogyakarta", "Gaming!")
console.log(perkenalkanDiri)


