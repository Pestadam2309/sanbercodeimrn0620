function tampilkan() {
    console.log("halo!")
}

tampilkan();

function munculkanAngka2() {
    return 2
}

var tampung = munculkanAngka2();
console.log(tampung)

function kalikan2(angka) {
    return angka * 2
}

var tampung2 = kalikan2(2);
console.log(tampung2)

function tampilkanAngka(angka1, angka2) {
    return angka1 + angka2
}

console.log(tampilkanAngka(5, 3))

function tampilkanAngka2(angka = 1) {
    return angka
}

console.log(tampilkanAngka2(5))
console.log(tampilkanAngka2())

var fungsiPerkalian = function(angka1, angk2) {
    return angka1 * angk2
}

console.log(fungsiPerkalian(2, 4))

// deklarasi function tanpa return
function hello() {
    console.log("Hello Pesta");
}

var hello2 = function() {
    console.log("Hello Pesta 2")
}
// //jalankan function
hello()
hello2()


// function dengan return
function greet() {
    var string = "Haloo saya Pesta"
    return string
}
 function greet2(greeter) {
     var string = " Halo dengan " + greeter + " di Sini ada yang bisa saya bantu?"
     return string;
 }
function greet3(greeter, time) {
    var string = "Halo Selamat " +time + " ! Dengan " + greeter + " disini"
    return string
}

var sapa = greet2("Abduh")
var sapa2 = greet2("Pesta")
var sapa3 = greet3("Jane", "siang")

console.log(sapa)
console.log(sapa2)
console.log(sapa3)



